package com.example.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class ProductQuantity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
    private String lotNumber;
    private Long quantityInGrams;

    public ProductQuantity(Product product, String lotNumber, Long quantityInGrams) {
        this.product = product;
        this.lotNumber = lotNumber;
        this.quantityInGrams = quantityInGrams;
    }
}
