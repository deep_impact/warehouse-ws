package com.example.repository;

import com.example.entity.Product;
import com.example.entity.ProductQuantity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductQuantityRepository extends CrudRepository<ProductQuantity, Long> {
    List<ProductQuantity> findByProduct(@Param("product") Product product);
}
