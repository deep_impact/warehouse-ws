package com.example.repository;

import com.example.entity.Product;
import com.example.entity.ProductQuantity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ProductQuantityRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductQuantityRepository target;


    @Test
    public void testFindByProduct() {

        final Product apple = new Product("APPLE", "Apple");
        final Product pear = new Product("PEAR", "Pear");
        entityManager.persist(apple);
        entityManager.persist(pear);

        entityManager.persist(new ProductQuantity(apple, "2022-07-05-010", 3000L));
        entityManager.persist(new ProductQuantity(apple, "LOT # B-01580609", 7000L));
        entityManager.persist(new ProductQuantity(pear, "LOT # B-01580609", 7000L));

        final List<ProductQuantity> apples = target.findByProduct(apple);
        assertEquals(2, apples.size());
    }
}
